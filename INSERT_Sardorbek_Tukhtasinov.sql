INSERT INTO film (
        title, 
        description, 
        release_year, 
        language_id, 
        rental_duration, 
        rental_rate, 
        length, 
        replacement_cost, 
        rating, 
        special_features
    )
    VALUES (
        'The Fast and The Furious', 
        'Fast & Furious is an American media franchise centered on a series of action films that are largely concerned with street racing, heists, spies, and family.', 
        2001, 
        1, 
        14, 
        4.99, 
        106, 
        19.99, 
        'PG-13'::mpaa_rating, 
        ARRAY['Tutorials']
    );
-- 2.1) Insert actors into the actor table
INSERT INTO actor (first_name, last_name)
VALUES ('Vin', 'Diesel'), ('Paul', 'Walker'), ('Michelle', 'Rodriguez');
-- 2.2) Insert film actor
INSERT INTO film_actor (actor_id, film_id)
SELECT actor.actor_id, film.film_id
FROM film
INNER JOIN actor ON actor.last_name IN ('Diesel', 'Walker', 'Rodriguez')
WHERE film.title = 'The Fast and The Furious';
-- 3) Insert the film into the inventory
INSERT INTO inventory (film_id, store_id)
SELECT f.film_id, 1
FROM film f
WHERE f.title = 'The Fast and The Furious';


-- To run the project select all the code